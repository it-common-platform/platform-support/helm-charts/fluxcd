## Flux CD Helm Chart

The manifests were created using the `flux` tool using the following command. The versions are dependent on your flux client version, so make sure you are using the correct one depending on your needs!

```shell
flux install --export \
    --components-extra=image-reflector-controller,image-automation-controller \
    --namespace platform-flux-system > templates/manifests.yml
```

We're still using it as a Helm chart to allow for easier rollback/upgrades
